package com.example.BTP.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.BTP.model.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
    @Query(value = "SELECT * FROM account", nativeQuery = true)
	Optional<Account> findById(long id);

    @Query(value = "SELECT * FROM account WHERE id_account = :id", nativeQuery = true)
Account findById2(@Param("id") long id);

}
