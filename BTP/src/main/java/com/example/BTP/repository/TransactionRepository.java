package com.example.BTP.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.BTP.model.Account;
import com.example.BTP.model.Transaction;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    @Query("SELECT COALESCE(SUM(t.balance), 0) FROM Transaction t WHERE t.accountId.id = :accountId")
Double findLastBalanceByAccountId(@Param("accountId") long accountId);

@Query("SELECT t FROM Transaction t WHERE t.accountId = :account AND t.transactionDate BETWEEN :startDate AND :endDate")
List<Transaction> findByAccountIdAndTransactionDateBetween(
        @Param("account") Account account,
        @Param("startDate") Date startDate,
        @Param("endDate") Date endDate);
}
