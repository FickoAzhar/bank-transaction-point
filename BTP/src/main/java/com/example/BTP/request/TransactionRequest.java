package com.example.BTP.request;

import java.util.Date;

public class TransactionRequest {
    private long accountId;
    private Date transactionDate;
    private String description;
    private String debitCreditStatus;
    private double amount;

    public long getAccountId() {
        return accountId;
    }
    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }
    public Date getTransactionDate() {
        return transactionDate;
    }
    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDebitCreditStatus() {
        return debitCreditStatus;
    }
    public void setDebitCreditStatus(String debitCreditStatus) {
        this.debitCreditStatus = debitCreditStatus;
    }
    public double getAmount() {
        return amount;
    }
    public void setAmount(double amount) {
        this.amount = amount;
    }
    
    
}
