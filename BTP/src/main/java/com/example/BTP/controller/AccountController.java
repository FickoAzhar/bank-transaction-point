package com.example.BTP.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BTP.model.Account;
import com.example.BTP.repository.AccountRepository;
import com.example.BTP.repository.TransactionRepository;

@RestController
@RequestMapping("/api/customer")
public class AccountController {
    @Autowired
    TransactionRepository transactionRepo;

    @Autowired
    AccountRepository accountRepo;

    @PostMapping("/create")
    public ResponseEntity<Object> createemployee(@RequestBody Account body) {
        try {
        Map<String, Object> result = new HashMap<>();
        Account account = accountRepo.save(body);
        HttpStatus status = HttpStatus.CREATED;
        result.put("status", "201");
        result.put("message", "Account created successfully!");
        result.put("data", account);
        return new ResponseEntity<Object>(result, status);
        } catch (Exception e) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
