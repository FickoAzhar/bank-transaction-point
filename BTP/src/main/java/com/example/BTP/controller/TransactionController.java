package com.example.BTP.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BTP.model.Account;
import com.example.BTP.model.Transaction;
import com.example.BTP.repository.AccountRepository;
import com.example.BTP.repository.TransactionRepository;
import com.example.BTP.request.TransactionRequest;

@RestController
@RequestMapping("/api/transaction")
public class TransactionController {
    @Autowired
    AccountRepository accountRepo;

    @Autowired
    TransactionRepository transactionRepo;


    @PostMapping("/create")
    public ResponseEntity<String> createTransaction(@RequestBody TransactionRequest request) {
        try {
            Account account = accountRepo.findById(request.getAccountId())
                .orElseThrow();

            Transaction transaction = new Transaction();
            double lastBalance = transactionRepo.findLastBalanceByAccountId(request.getAccountId());
            transaction.setAccountId(account);
            transaction.setTransactionDate(request.getTransactionDate());
            transaction.setDescription(request.getDescription());
            transaction.setDebitCreditStatus(request.getDebitCreditStatus());
            transaction.setAmount(request.getAmount());

            if(!request.getDebitCreditStatus().equalsIgnoreCase("debit") && !request.getDebitCreditStatus().equalsIgnoreCase("kredit")){
                return ResponseEntity.badRequest().body("Transaction type must be credit and debit");
            } 
            // else if (request.getDebitCreditStatus().equalsIgnoreCase("debit") && accs.getBalance() < request.getAmount())
            else if(request.getDebitCreditStatus().equalsIgnoreCase("debit")){
                transaction.setBalance(lastBalance - request.getAmount());
                if(request.getDescription().equalsIgnoreCase("beli pulsa")){
                    double amount = request.getAmount();
                    if(amount >= 0 && amount <= 10000){
                        account.setPoint(0);
                    } else if (amount >= 10001 && amount <= 30000){
                        double originalValue = amount/1000;
                        int integerValue = (int) originalValue;
                        double doubleValue = (double) integerValue;
                        account.setPoint(doubleValue + account.getPoint());
                    } else{
                        double originalValue = amount/1000;
                        int integerValue = (int) originalValue;
                        double doubleValue = (double) integerValue;
                        account.setPoint((doubleValue * 2) + account.getPoint());
                    }
                } else if (request.getDescription().equalsIgnoreCase("bayar listrik")){
                    double amount = request.getAmount();
                    if(amount >= 0 && amount <= 50000){
                        account.setPoint(0);
                    } else if (amount >= 50001 && amount <= 100000){
                        double originalValue = amount/2000;
                        int integerValue = (int) originalValue; 
                        double doubleValue = (double) integerValue; 
                        account.setPoint(doubleValue + account.getPoint());
                    } else{
                        double originalValue = amount/2000;
                        int integerValue = (int) originalValue; 
                        double doubleValue = (double) integerValue; 
                        account.setPoint((doubleValue * 2) + account.getPoint());
                    }
                }
            } else {
                transaction.setBalance(lastBalance + request.getAmount());
            }
            transactionRepo.save(transaction);
            accountRepo.save(account);

            return ResponseEntity.ok("Transaction created successfully!");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error creating transaction");
        }
    }
}
