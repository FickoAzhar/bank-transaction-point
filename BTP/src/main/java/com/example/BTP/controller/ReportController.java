// package com.example.BTP.controller;

// public class ReportController {
    
// }


package com.example.BTP.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BTP.model.Account;
import com.example.BTP.model.Transaction;
import com.example.BTP.repository.AccountRepository;
import com.example.BTP.repository.TransactionRepository;

import java.util.Date;
import java.util.List;

@RestController
public class ReportController {

    @Autowired
    private TransactionRepository transactionRepo;

    @Autowired
    private AccountRepository accountRepo;

    @GetMapping("/api/customer/report")
    public ResponseEntity<List<Transaction>> generateAccountStatement(
            @RequestParam Long accountId,
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date startDate,
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date endDate) {

        Account acc = accountRepo.findById2(accountId);

        List<Transaction> transactions = transactionRepo.findByAccountIdAndTransactionDateBetween(
                acc, startDate, endDate);

        return ResponseEntity.ok(transactions);
    }
}
