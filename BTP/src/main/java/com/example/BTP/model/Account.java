package com.example.BTP.model;

import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "account", schema = "public")
public class Account {
    private long id;
	private String customerName;
    private double point;
    private Set<Transaction> transactions = new HashSet<Transaction>(0);
    
    public Account() {
    }

    public Account(long id, String customerName, double point) {
        this.id = id;
        this.customerName = customerName;
        this.point = point;
    }

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_account")
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "name", nullable = false)
    public String getCustomerName() {
        return customerName;
    }
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    @OneToMany(mappedBy = "accountId", cascade = CascadeType.ALL)
    public Set<Transaction> getTransactions() {
        return transactions;
    }
    public void setTransactions(Set<Transaction> transactions) {
        this.transactions = transactions;
    }

    @Column(name = "point", nullable = true)
    public double getPoint() {
        return point;
    }

    public void setPoint(double point) {
        this.point = point;
    }
    
}
