package com.example.BTP.model;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "transaction", schema = "public")
public class Transaction {
    private long id;
    private Account AccountId;
    private Date TransactionDate;
    private String Description;
    private String DebitCreditStatus;
    private double Amount;
    private double Balance;
    
    public Transaction() {
    }

    public Transaction(long id, Account accountId, Date transactionDate, String description, String debitCreditStatus,
            double amount, double balance) {
        this.id = id;
        AccountId = accountId;
        TransactionDate = transactionDate;
        Description = description;
        DebitCreditStatus = debitCreditStatus;
        Amount = amount;
        Balance = balance;
    }

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_transaction")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_account")
    public Account getAccountId() {
        return AccountId;
    }

    public void setAccountId(Account accountId) {
        AccountId = accountId;
    }

    @Column(name = "transaction_date", nullable = false)
    public Date getTransactionDate() {
        return TransactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        TransactionDate = transactionDate;
    }

    @Column(name = "description", nullable = false)
    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    @Column(name = "debit_credit_status", nullable = false)
    public String getDebitCreditStatus() {
        return DebitCreditStatus;
    }

    public void setDebitCreditStatus(String debitCreditStatus) {
        DebitCreditStatus = debitCreditStatus;
    }

    @Column(name = "amount", nullable = false)
    public double getAmount() {
        return Amount;
    }

    public void setAmount(double amount) {
        Amount = amount;
    }

    @Column(name = "balance", nullable = false)
    public double getBalance() {
        return Balance;
    }

    public void setBalance(double balance) {
        Balance = balance;
    }
}
